#!/usr/bin/env sh

source ./bin/utils/log.sh

if [ -n "$PAC_LOGIN" ] && [ -n "$PAC_API_KEY" ]; then
  info "PAC_LOGIN and PAC_API_KEY will be used to login to NPM registry."
  curl -u $PAC_LOGIN:$PAC_API_KEY https://packages.atlassian.com/api/npm/atlassian-npm/auth/atlassian > .npmrc
elif [ -n "$NPM_TOKEN" ]; then
  info "NPM_TOKEN is set. Configure .npmrc file manually"
  npm set @atlassian:registry "https://packages.atlassian.com/api/npm/atlassian-npm/"
  npm set //packages.atlassian.com/api/npm/atlassian-npm/:_authToken=${NPM_TOKEN}
elif [ -f "$HOME/.npmrc" ]; then
  info "Trying to use .npmrc file from ${HOME}"
  cp "$HOME/.npmrc" .
else
  warn "Please provide at least PAC_LOGIN and PAC_API_KEY or NPM_TOKEN in the environment variable."
  error "Cannot setup npm credential for you."
  exit 1
fi;

npm install --unsafe-perm
