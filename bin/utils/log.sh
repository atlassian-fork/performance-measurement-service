#!/usr/bin/env sh

GREEN='\033[0;32m'
YELLOW='\033[0;33m'
RED='\033[0;31m'
NC='\033[0m' # No Color

info () {
  printf "${GREEN} $1 ${NC} \n"
}

warn () {
  printf "${YELLOW} $1 ${NC} \n"
}

error () {
  printf "${RED} $1 ${NC} \n"
}
