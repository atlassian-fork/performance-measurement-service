import Metrics from '@atlassian/metrics';
import { Context } from 'koa';

const NS_PER_MILLI_SEC = 1e6;
const MILLI_SEC_PER_SEC = 1e3;
const SLA_MILLIS = 20;

type HiResTime = [number, number];

export default function(metrics: Metrics) {
  return async function wrapperMiddleware(ctx: Context, next: () => Promise<any>) {
    const time = process.hrtime();
    if (ctx.request.length) {
      metrics.timing('request.body-length', ctx.request.length);
    }
    await next();

    const diff: HiResTime = process.hrtime(time);
    const totalResponseTimeInMilliSeconds = diff[0] * MILLI_SEC_PER_SEC + diff[1] / NS_PER_MILLI_SEC;
    const tags = [
      `http-status:${ctx.status}`,
      `http-method:${ctx.method}`,
      `endpoint:${ctx.state[Symbol.for('endpoint')] || 'endpoint-not-found'}`,
      `meets-sla:${totalResponseTimeInMilliSeconds <= SLA_MILLIS}`
    ];

    // A timing metric includes a counter:
    // https://extranet.atlassian.com/display/~hmurn/2018/04/22/Datadoggie+treats+1%3A+timers+and+counters
    // So it is sufficient to get the request.time.count for the tags we're interested in
    metrics.timing('request.time', totalResponseTimeInMilliSeconds, undefined, tags);
  };
}
