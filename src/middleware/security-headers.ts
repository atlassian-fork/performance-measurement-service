import { Context } from 'koa';

export default async (ctx: Context, next: () => Promise<any>) => {
  ctx.set({
    'Strict-Transport-Security': 'max-age=31536000;',
    'X-Content-Type-Options': 'nosniff',
    'X-Frame-Options': 'Deny',
    'X-XSS-Protection': '1; mode=block'
  });
  await next();
};
