import Metrics from '@atlassian/metrics';
import { Context } from 'koa';

export interface MetricsContext {
  metrics?: Metrics;
}

export default (metrics: Metrics) => {
  return async (ctx: Context & MetricsContext, next: () => Promise<any>) => {
    ctx.metrics = metrics;
    await next();
  };
};
