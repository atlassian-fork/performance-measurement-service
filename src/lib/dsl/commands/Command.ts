import { Browser, Page } from 'puppeteer';

// TODO: Pass previous context to the next command
// tslint:disable-next-line:no-empty-interface
export interface Context {}

export interface Options {
  baseUrl: string;
}

export default interface Command {
  execute(page: Page, browser: Browser, options: Options, context: Context): Promise<Context>;
}
