import { Page } from 'puppeteer';
import Command from './Command';

export default class InsertAdf implements Command {
  adf: string;

  constructor(adf?: string) {
    if (!adf) {
      throw new Error('Required argument `adf` for `adf_insert` is missing');
    }
    this.adf = adf;
  }

  async execute(page: Page) {
    await page.waitForSelector('#adf-input');
    await page.click('#adf-input');
    await page.type('#adf-input', JSON.stringify(this.adf));
    await page.click('#import-adf');

    return {};
  }
}
