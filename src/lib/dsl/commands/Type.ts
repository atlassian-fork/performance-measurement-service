import { Page } from 'puppeteer';
import Command from './Command';

export default class Type implements Command {
  text: string;

  constructor(text?: string) {
    if (!text) {
      throw new Error('Required argument `text` for `Type` is missing');
    }
    this.text = text;
  }

  async execute(page: Page) {
    await page.waitForSelector('.ProseMirror');
    await page.click(`.ProseMirror`);
    await page.type('.ProseMirror', `${this.text}`);

    return {};
  }
}
