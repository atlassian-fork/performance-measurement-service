import { Page } from 'puppeteer';
import Command from './Command';

export default class Click implements Command {
  selector: string;
  rowIndex?: number;
  columnIndex?: number;

  constructor(selector?: string, rowIndex?: number, columnIndex?: number) {
    if (!selector) {
      throw new Error('Required argument `selector` for `click` is missing');
    }
    this.selector = selector;
    this.rowIndex = rowIndex;
    this.columnIndex = columnIndex;
  }

  async execute(page: Page) {
    if (this.selector === 'cell') {
      const { rowIndex, columnIndex } = this;
      await page.waitForSelector('.ProseMirror table');
      await page.click(
        `tr:nth-child(${rowIndex}) td:nth-child(${columnIndex}), tr:nth-child(${rowIndex}) th:nth-child(${columnIndex})`
      );
    } else {
      await page.waitForSelector(this.selector);
      await page.click(this.selector);
    }

    return {};
  }
}
