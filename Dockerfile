FROM alekzonder/puppeteer
WORKDIR /app
ARG PAC_LOGIN
ARG PAC_API_KEY
ARG NPM_TOKEN

USER root
RUN apt-get update && apt-get install -y build-essential
USER pptruser

COPY package.json .
COPY package.json.d.ts .
COPY package-lock.json .
COPY typings typings
COPY tsconfig.json .
COPY tslint.json .
COPY config.default.json .
COPY bin bin
COPY src src

RUN ./bin/utils/npm-install.sh \
  && npm run build \
  && npm prune --production

# Tell Puppeteer to skip installing Chrome. We'll be using the installed package.
ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD true

EXPOSE 8080
CMD ["npm", "start"]
